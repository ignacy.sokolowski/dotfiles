[Appearance]
ColorScheme=ignacy

[General]
Icon=utilities-terminal
LocalTabTitleFormat=%d : %n
MenuIndex=1
Name=Powłoka
Parent=FALLBACK/
RemoteTabTitleFormat=%h : %u
StartInCurrentSessionDir=false

[Scrolling]
HistoryMode=2
